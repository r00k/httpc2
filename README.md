# httpC2

#### Requirements
Both the server and client were written using Python 3. Server makes use of a
venv called v-httpc2, which you will need to recreate for the server to work out 
of the box using python run.py

`python3 -m venv v-httpc2`

Server requires flask

`pip install flask`

Client requires requests

`pip install requests`

Uses cookies as messaging transport for c2. Client makes a get request
to the server, which then replies with any saved commands for the client. The
client runs the commands and returns the command's output with the next beacon
attempt.


Upgrade thoughts:

*needs a name

*actually format this damned readme file...cause it's ugly

*server embeds all pending commands for the client into the body of the response rather than one command at a time using cookies

*rather than base64, use encryption on the cookie values so they're encrypted even if victim network uses https mitm

*get https working between clients and server (self-signed cert should be fine)

*use random padding on the get requests and responses to vary message length to foil beacon detection

*mongo backend for server for command queuing and results tracking

*setup queuing system for client to track pending commands

*write request and response fragmentation functions to prevent cookies that are too long (maybe, need to discuss if longer cookies helps vary length for detection avoidance)

*write client to use sockets or urllib to rid dependance on requests library

*write powershell client

*write compiled clients

*write Pylon module to enable compatibility with NexusC2

*update client to continually check in with c2, using beacon function if no command results need to be returned and return_command if there is.

*update client to use a different, common user agent. 

*create "kill" command that, when received by the client, will clean itself from the victim machine.

*Restore longer wait times to client to provide more varied beacon times to avoid beacon frequency detection. Should be 1-20 minute base with a 0-5 minute addition

*upgrade for client key: introduce some sort of hashing so that client key always changes but is predictable with some known value. prevent c2 replay where an incident responder or analyst can access the c2 server with only observed packets

*add proxy support to client for "reverse shell" through proxy functionality

