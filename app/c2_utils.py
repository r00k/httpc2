'''
data structure for saved files (maybe mongo database later). saved files are named client ids:
{"command1": {"time1": "result"}, "command2": {"time1": "result", "time2": "result"}}
'''

def get_file_dict(file_path):
'''
takes in a path to an expected json file, returns the json from
the file or an empty dict to begin json file.
'''
    try:
        with open(file_path) as f:
            return json.load(f)
    except:
        return {}

def write_results_to_file(client, command, result):
'''
takes in c2 command result data and writes to a file.
Each client has its own file, and command output is structured by time
{"command1": {"time1": "result"}, "command2": {"time1": "result", "time2": "result"}}
'''
    target_file = "files/{0}".format(client)
    jdata = get_file_dict(target_file)
    if command in jsoned.keys():
        jdata[command][str(int(time()))] = result
    else:
        jdata[command] = {str(int(time())): result}
    with open(target_file, "w"):
        json.dump(jdata)
